#include <stdbool.h>
#include <stdio.h>
#include "heap.h"
#include "io.h"

int main() {

	Heap* fila = HeapCria();

	/*HeapInsertRaw(fila, 12, 5);
	HeapInsertRaw(fila, 15, 3);
	HeapInsertRaw(fila, 2, 0);
	HeapInsertRaw(fila, 48, 1);
	HeapInsertRaw(fila, 89, 5);
	HeapInsertRaw(fila, 17, 3);
	HeapInsertRaw(fila, 82, 7);
	HeapInsertRaw(fila, 62, 10);
	HeapInsertRaw(fila, 42, 9);
	HeapInsertRaw(fila, 51, 4);*/

	while (true) {

		PrintLine("");
		PrintLine("0 - Sair");
		PrintLine("1 - Inserir");
		PrintLine("2 - Remover");
		PrintLine("3 - Consultar");
		PrintLine("4 - Imprimir Fila");
		PrintLine("");

		int opt = ReadInt("Insira a op��o: ");

		int value, prior;
		HeapElement item;

		switch (opt) {
			case 0:
				HeapLibera(fila);
				return 0;

			case 1:
				value = ReadInt("Valor: ");
				prior = ReadInt("Prioridade: ");
				HeapInsertRaw(fila, value, prior);
				break;

			case 2:
				if (fila->size > 0) {
					PrintLine("");
					item = HeapPop(fila);
					HeapPrint(fila);
					printf("Removido valor %d (Prioridade %d)\n", item.value, item.prioridade);
				}
				else
					PrintLine("Fila vazia");
				break;

			case 3:
				value = ReadInt("Valor para consultar: ");
				HeapConsulta(fila, value);
				break;

			case 4:
				PrintLine("");
				printf("%d items e capacidade para %d\n", fila->size, fila->capacity);
				HeapPrint(fila);
				break;

			default:
				PrintLine("Op��o inv�lida");
				break;
		}

	}

	return 0;
}

