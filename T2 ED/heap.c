#include <stdlib.h>
#include <stdio.h>
#include "heap.h"
#include "io.h"
#define DEFAULT_CAPACITY 1

Heap* HeapCria() {
	Heap* heap = malloc(sizeof(Heap));

	heap->capacity = DEFAULT_CAPACITY;
	heap->size = 0;
	heap->elements = malloc(heap->capacity * sizeof(HeapElement));

	return heap;
}
	
void HeapLibera(Heap* heap) {
	free(heap->elements);
	free(heap);
}

int HeapEsqIndex(int pai) { return 2 * pai + 1; }
int HeapDirIndex(int pai) { return 2 * pai + 2; }
int HeapPaiIndex(int index) { return (index - 1) / 2; }

bool HeapTemEsq(Heap* heap, int index) { return HeapEsqIndex(index) < heap->size; }
bool HeapTemDir(Heap* heap, int index) { return  HeapDirIndex(index) < heap->size; }
bool HeapTemPai(int index) { return HeapPaiIndex(index) >= 0; }

HeapElement HeapEsq(Heap* heap, int index) { return heap->elements[HeapEsqIndex(index)]; }
HeapElement HeapDir(Heap* heap, int index) { return heap->elements[HeapDirIndex(index)]; }
HeapElement HeapPai(Heap* heap, int index) { return heap->elements[HeapPaiIndex(index)]; }

void HeapTroca(Heap* heap, int indexA, int indexB) {
	HeapElement aux = heap->elements[indexA];
	heap->elements[indexA] = heap->elements[indexB];
	heap->elements[indexB] = aux;
}

void HeapExpande(Heap* heap) {
	if (heap->size == heap->capacity) {
		HeapElement* old = heap->elements;
		heap->elements = malloc(heap->capacity * 2 * sizeof(HeapElement));

		for (int i = 0; i < heap->capacity; i++)
			heap->elements[i] = old[i];

		heap->capacity *= 2;
		free(old);
	}
}

HeapElement HeapPop(Heap* heap) {
	if (heap->size == 0) {
		PrintLine("N�o h� elementos no heap");
		return (HeapElement) { 0 };
	}

	HeapElement item = heap->elements[0];
	heap->elements[0] = heap->elements[--heap->size];
	HeapHeapifyBaixo(heap);

	return item;
}

void HeapInsertRaw(Heap* heap, int value, int prioridade) {
	HeapElement element;
	element.prioridade = prioridade;
	element.value = value;
	HeapInsert(heap, element);
}

void HeapInsert(Heap* heap, HeapElement element) {
	HeapExpande(heap);
	heap->elements[heap->size++] = element;
	HeapHeapifyCima(heap);
}

void HeapHeapifyBaixo(Heap* heap) {
	int index = 0;

	while (HeapTemEsq(heap, index)) {
		int maiorFilho = HeapEsqIndex(index);

		if (HeapTemDir(heap, index) && HeapDir(heap, index).prioridade > HeapEsq(heap, index).prioridade)
			maiorFilho = HeapDirIndex(index);

		if (heap->elements[index].prioridade > heap->elements[maiorFilho].prioridade)
			break;

		HeapTroca(heap, index, maiorFilho);
		index = maiorFilho;
	}
}

void HeapHeapifyCima(Heap* heap) {
	int index = heap->size - 1;

	while (HeapTemPai(index) && HeapPai(heap, index).prioridade < heap->elements[index].prioridade) {
		HeapTroca(heap, HeapPaiIndex(index), index);
		index = HeapPaiIndex(index);
	}
}

void HeapPrint(Heap* heap) {
	for (int i = 0; i < heap->size; i++)
		printf("Valor: %d\tPrioridade: %d\n", heap->elements[i].value, heap->elements[i].prioridade);
}

void HeapConsulta(Heap* heap, int value) {
	bool found = false;

	for (int i = 0; i < heap->size; i++)
		if (heap->elements[i].value == value) {
			printf("Valor: %d\tPrioridade: %d\n", heap->elements[i].value, heap->elements[i].prioridade);
			found = true;
		}

	if (!found)
		PrintLine("Valor n�o encontrado");
}
