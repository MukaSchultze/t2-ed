#pragma once
#include <stdbool.h>

typedef struct heapElement {
	int value;
	int prioridade;
} HeapElement;

typedef struct heap {
	int size;
	int capacity;
	HeapElement* elements;
} Heap;

Heap* HeapCria();
void HeapLibera(Heap* heap);

int HeapEsqIndex(int pai);
int HeapDirIndex(int pai);
int HeapPaiIndex(int index);

bool HeapTemEsq(Heap* heap, int index);
bool HeapTemDir(Heap* heap, int index);
bool HeapTemPai(int index);

HeapElement HeapEsq(Heap* heap, int index);
HeapElement HeapDir(Heap* heap, int index);
HeapElement HeapPai(Heap* heap, int index);

void HeapTroca(Heap* heap, int indexA, int indexB);
void HeapExpande(Heap* heap);

HeapElement HeapPop(Heap* heap);

void HeapInsertRaw(Heap* heap, int value, int prioridade);
void HeapInsert(Heap* heap, HeapElement element);

void HeapHeapifyBaixo(Heap* heap);
void HeapHeapifyCima(Heap* heap);

void HeapPrint(Heap* heap);
void HeapConsulta(Heap* heap, int value);